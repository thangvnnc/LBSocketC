//
//  SocketClient.c
//  LBSocketC
//
//  Created by CPU-M03 on 1/10/18.
//  Copyright © 2018 CPU-M03. All rights reserved.
//

#include "SocketClient.h"

int socket_connect(int *sockfd, char *host, int port)
{
    int rs_connect;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    
    *sockfd = socket(AF_INET, SOCK_STREAM, 0);
    // Kiểm tra open sockfd: thất bại
    if (sockfd < 0)
    {
#if DEBUG
        printf("\n");
        perror("ERR_OPENNING_SOCKET");
#endif
        return ERR_OPENNING_SOCKET;
    }
    
    server = gethostbyname(host);
    // Kiểm tra lấy địa chỉ từ host thất bại
    if (server == NULL)
    {
#if DEBUG
        printf("\n");
        perror("ERR_GET_HOSTNAME");
#endif
        return ERR_GET_HOSTNAME;
    }
    
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(port);
    
    rs_connect = connect(*sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr));
    // Kiểm tra kết nối với server: thất bại
    if (rs_connect < 0)
    {
#if DEBUG
        printf("\n");
        perror("ERR_CONNECT_SERVER");
#endif
        return ERR_CONNECT_SERVER;
    }
#if DEBUG
    printf("\n");
    printf("SUC_CONNECT_SERVER with sockfd: %d", *sockfd);
#endif
    
    return SUC_CONNECT_SERVER;
}

void runClientDemo()
{
    char *host = "127.0.0.1";
    int port = 8000;
    int rs_connect, sockfd, rs_recv;
    
    rs_connect = socket_connect(&sockfd, host, port);
    if (rs_connect != SUC_CONNECT_SERVER)
    {
        close(sockfd);
        return;
    }
    
    char buff[1024];
    ssize_t len_recv = 0;
    ssize_t lentemp = 9;
    memset( &buff, '\0', sizeof(buff));
    rs_recv = socket_recv(sockfd, buff, lentemp, &len_recv);
    if (rs_recv != SUC_RECV_DATA)
    {
        close(sockfd);
        return;
    }
    
    close(sockfd);
}
