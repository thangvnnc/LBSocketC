//
//  Define.h
//  LBSocketC
//
//  Created by CPU-M03 on 1/11/18.
//  Copyright © 2018 CPU-M03. All rights reserved.
// "//" của server
// "///" của client
// "////" cả 2

#ifndef Define_h
#define Define_h

#define SOCKET_BACKLOG                      128//

// Success
#define SUC_LISTEN_SOCKET                   200000//
#define SUC_ACCEPT_CLIENT                   200001//
#define SUC_CONNECT_SERVER                  200002///
#define SUC_SEND_DATA                       200003///
#define SUC_RECV_DATA                       200004///
#define SUC_GET_POST_AVAILABLE              200005///
#define SUC_START_SERVER                    200006///
#define SUC_CLOSE_SOCKET                    200007///

// Error
#define ERR_OPENNING_SOCKET                 100000////
#define ERR_ON_BINDING                      100001//
#define ERR_LISTENING_SOCKET                100002//
#define ERR_ON_ACCEPT                       100003//
#define ERR_SEND_DATA                       100004//
#define ERR_GET_HOSTNAME                    100005///
#define ERR_CONNECT_SERVER                  100006///
#define ERR_RECV_DATA                       100007////
#define ERR_PORT_BUSY                       100008////
#define ERR_NOT_BIND                        100009////
#define ERR_GET_SOCK_NAME                   1000010////
#define ERR_CLOSE_CONNECT                   1000011////

#endif /* Define_h */
