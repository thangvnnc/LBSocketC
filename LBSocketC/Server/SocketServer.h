//
//  SocketServer.h
//  LBSocketC
//
//  Created by CPU-M03 on 1/10/18.
//  Copyright © 2018 CPU-M03. All rights reserved.
//

#ifndef SocketServer_h
#define SocketServer_h

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <dirent.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/select.h>
#include <LBSocketC/Define.h>
#include <LBSocketC/Socket.h>

int socket_listen(int port, int *sockfd);
int socket_accept(int sockfd, int *sockfd_cli);
int start_server(int port, int *sockfd);
void runServerDemo(void);

#endif /* SocketServer_h */


