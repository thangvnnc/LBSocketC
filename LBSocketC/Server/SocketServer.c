#include "SocketServer.h"

// Bắt đầu lắng nghe socket
int socket_listen(int port, int *sockfd)
{
    struct sockaddr_in serv_addr;

    /// Thông tin func https:///msdn.microsoft.com/en-us/library/windows/desktop/ms740506(v=vs.85).aspx
    *sockfd = socket(AF_INET, SOCK_STREAM, 0);

    // Kiểm tra tạo sockfd: thất bại
    if (sockfd < 0)
    {
#if DEBUG
        printf("\n");
        perror("ERR_OPENNING_SOCKET");
#endif
        return ERR_OPENNING_SOCKET;
    }

    /// set struct sockaddr_in
    bzero((char *) &serv_addr, sizeof(serv_addr));

    /// set value struct sockaddr_in
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(port);

    /// ràng buộc socket với host
    int bindValue = bind(*sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));

    // Kiểm tra ràng buộc: thất bại
    if (bindValue < 0)
    {
#if DEBUG
        printf("\n");
        perror("ERR_ON_BINDING");
#endif
        return ERR_ON_BINDING;
    }

    ///
    /// Lắng nghe kết nối đến từ client
    /// BACKLOG là giá số hạn đợi tối đa của conenction (giới hạn số lượng chờ kết nối)
    /// https:///msdn.microsoft.com/en-us/library/windows/desktop/ms739168(v=vs.85).aspx
    ///
    int listenValue = listen(*sockfd, SOCKET_BACKLOG);

    // Kiểm tra lắng nghe kết nối: thất bại
    if (listenValue != 0)
    {
#if DEBUG
        printf("\n");
        perror("ERR_LISTENING_SOCKET");
#endif
        return ERR_LISTENING_SOCKET;
    }

#if DEBUG
    printf("\n");
    printf("SUC_LISTEN_SOCKET");
#endif

    return SUC_LISTEN_SOCKET;
}

int socket_accept(int sockfd, int *sockfd_cli)
{
    socklen_t clilen;
    struct sockaddr_in cli_addr;
    clilen = sizeof(cli_addr);
    *sockfd_cli = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

    // Kiểm tra chấp nhận kết nối: thất bại
    if (*sockfd_cli < 0)
    {
#if DEBUG
        printf("\n");
        perror("ERR_ON_ACCEPT");
#endif
        return ERR_ON_ACCEPT;
    }

#if DEBUG
    printf("\n");
    printf("SUC_ACCEPT_CLIENT with sockfd: %d", *sockfd_cli);
#endif

    return SUC_ACCEPT_CLIENT;
}

int start_server(int port, int *sockfd)
{
    int sfd, sockfd_cli;
    int rs_listen, rs_accept, rs_send, rs_checkport;
    int port_available = 0;
    
    rs_listen = socket_listen(port, &sfd);
    if (rs_listen != SUC_LISTEN_SOCKET)
    {
#if DEBUG
        printf("\n");
        printf("%d", rs_listen);
#endif
        close(sfd);
        return rs_listen;
    }
    
    *sockfd = sfd;
    
    while (1)
    {
#if DEBUG
        printf("\n");
        printf("%s", "TBD Handle client connect");
#endif
        rs_accept = socket_accept(sfd, &sockfd_cli);
        if (rs_accept != SUC_ACCEPT_CLIENT)
        {
#if DEBUG
            printf("\n");
            printf("%d", rs_accept);
#endif
            close(sfd);
            return rs_accept;
        }
        
        rs_checkport = socket_get_port_available(&port_available);
        if (rs_checkport != SUC_GET_POST_AVAILABLE)
        {
#if DEBUG
            printf("\n");
            printf("%d", rs_checkport);
#endif
            close(sfd);
            return rs_checkport;
        }
        printf("\n");
        printf("port available: %d", port_available);
    }
    
    return SUC_START_SERVER;
}

void runServerDemo()
{
    int port = 8000;
    int sockfd, sockfd_cli;
    int rs_listen, rs_accept, rs_send;

    rs_listen = socket_listen(port, &sockfd);
    if (rs_listen != SUC_LISTEN_SOCKET)
    {
        close(sockfd);
        return;
    }

    rs_accept = socket_accept(sockfd, &sockfd_cli);
    if (rs_accept != SUC_ACCEPT_CLIENT)
    {
        close(sockfd);
        return;
    }

    char *buff = "123456789";
    ssize_t len_send = 0;
    ssize_t lentemp = 9;
    rs_send = socket_send(sockfd_cli, buff, lentemp, &len_send);
    if (rs_send != SUC_SEND_DATA)
    {
        close(sockfd);
        return;
    }

    close(sockfd_cli);
    close(sockfd);
}
