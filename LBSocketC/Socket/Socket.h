//
//  Socket.h
//  LBSocketC
//
//  Created by CPU-M03 on 1/11/18.
//  Copyright © 2018 CPU-M03. All rights reserved.
//

#ifndef Socket_h
#define Socket_h

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <dirent.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/select.h>
#include <LBSocketC/Define.h>

int socket_recv(int sockfd, char *buff, ssize_t buff_size, ssize_t *len_recv);
int socket_send(int sockfd, char *buff, ssize_t buff_size, ssize_t *len_send);
int socket_get_port_available(int *port);
int socket_close(int sockfd);

#endif /* Socket_h */
