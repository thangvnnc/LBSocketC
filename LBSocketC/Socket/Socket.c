//
//  Socket.c
//  LBSocketC
//
//  Created by CPU-M03 on 1/11/18.
//  Copyright © 2018 CPU-M03. All rights reserved.
//

#include "Socket.h"

int socket_recv(int sockfd, char *buff, ssize_t buff_size, ssize_t *len_recv)
{
    *len_recv = recv(sockfd, buff, buff_size, 0);
    if (*len_recv < 0)
    {
#if DEBUG
        printf("\n");;
        perror("ERR_RECV_DATA");
#endif
        return ERR_RECV_DATA;
    }
#if DEBUG
    printf("\n");;
    printf("SUC_RECV_DATA with: %zd", *len_recv);
#endif
    return SUC_RECV_DATA;
}

int socket_send(int sockfd, char *buff, ssize_t buff_size, ssize_t *len_send)
{
    /// int send(SOCKET s, const char *buf, int len, int flags);
    ///
    /// - parameter s:      Id sockfd
    /// - parameter buff:   Buffer cần gửi đi
    /// - parameter len:    Độ dài của buffer
    /// - parameter flags:  Biến cờ trạng thái nhận gửi dữ liệu
    ///
    /// Ý nghĩa flag của hàm send với recv
    ///
    /// OR gate:          AND gate:
    /// a b  out          a b  out
    /// 0 0  0            0 0  0
    /// 0 1  1            0 1  0
    /// 1 0  1            1 0  0
    /// 1 1  1            1 1  1
    ///
    
    *len_send = send(sockfd, buff, buff_size, 0);
    
    if (*len_send < 0)
    {
#if DEBUG
        printf("\n");;
        perror("ERR_SEND_DATA");
#endif
        return ERR_SEND_DATA;
    }
#if DEBUG
    printf("\n");
    printf("SUC_SEND_DATA with: %zd", *len_send);
#endif
    
    return SUC_SEND_DATA;
}

int socket_get_port_available(int *port)
{
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0)
    {
#if DEBUG
        printf("\n");
        printf("ERR_OPENNING_SOCKET");
#endif
        return ERR_OPENNING_SOCKET;
    }
    
    struct sockaddr_in serv_addr;
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = 0;
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    {
        if(errno == EADDRINUSE)
        {
#if DEBUG
            printf("\n");
            printf("ERR_PORT_BUSY");
#endif
            return ERR_PORT_BUSY;
        }
        else
        {
#if DEBUG
            printf("\n");
            printf("ERR_NOT_BIND");
#endif
            return ERR_NOT_BIND;
        }
    }
    
    socklen_t len = sizeof(serv_addr);
    if (getsockname(sockfd, (struct sockaddr *)&serv_addr, &len) == -1)
    {
#if DEBUG
        printf("\n");
        perror("ERR_GET_SOCK_NAME");
#endif
        return ERR_GET_SOCK_NAME;
    }
    
    *port = ntohs(serv_addr.sin_port);
    
    if (close (sockfd) < 0)
    {
#if DEBUG
        printf("\n");
        perror("ERR_CLOSE_CONNECT");
#endif
        return ERR_CLOSE_CONNECT;
    }
    
    return SUC_GET_POST_AVAILABLE;
}

int socket_close(int sockfd)
{
    if (close (sockfd) < 0)
    {
#if DEBUG
        printf("\n");
        perror("ERR_CLOSE_CONNECT");
#endif
        return ERR_CLOSE_CONNECT;
    }
    
    return SUC_CLOSE_SOCKET;
}
